package by.efectura.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name="sectors")
public class SectorModel {
    @Id
    @Column(name="sector_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Transient
    private boolean active;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "users_sectors",
            joinColumns = @JoinColumn(name = "sector_id", referencedColumnName = "sector_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"))
    private List<UserModel> users;

    @OneToMany
    @JoinColumn(name = "high_id")
    private List<SectorModel> subCategory;
}
