package by.efectura.repositories;

import by.efectura.models.SectorModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectorRepository extends CrudRepository<SectorModel, Long> {

    @Query(value = "SELECT * FROM sectors WHERE high_id is null", nativeQuery = true)
    List<SectorModel> findByParentIsNull();
}
