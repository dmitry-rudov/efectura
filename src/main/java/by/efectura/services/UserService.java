package by.efectura.services;

import by.efectura.models.UserModel;

public interface UserService {

    UserModel add(UserModel user);

    UserModel update(UserModel user);

    UserModel getBySession(String session);

    UserModel save(UserModel user, String session);
}
