package by.efectura.services;

import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;

import java.util.List;

public interface SectorService {
    List<SectorModel> getAll();

    List<SectorModel> getAllAndMarkActiveFor(UserModel user);
}
