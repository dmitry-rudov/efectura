package by.efectura.services.Impl;

import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;
import by.efectura.repositories.SectorRepository;
import by.efectura.services.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SectorServiceImpl implements SectorService{

    @Autowired
    private SectorRepository sectorRepository;

    private List<SectorModel> findSubCategory(SectorModel headNode) {
        List<SectorModel> subSectors = new ArrayList<SectorModel>();
        for (SectorModel sector : headNode.getSubCategory()) {
            subSectors.add(sector);
            if (sector.getSubCategory()!= null && sector.getSubCategory().size() > 0) {
                subSectors.addAll(findSubCategory(sector));
            }
        }
        return subSectors;
    }

    @Override
    public List<SectorModel> getAll() {
        List<SectorModel> headNodes = sectorRepository.findByParentIsNull();
        List<SectorModel> sortedSectorsByHierarchy = new ArrayList<SectorModel>();
        for (SectorModel headNode : headNodes) {
            sortedSectorsByHierarchy.add(headNode);
            sortedSectorsByHierarchy.addAll(findSubCategory(headNode));
        }

        return sortedSectorsByHierarchy;
    }

    @Override
    public List<SectorModel> getAllAndMarkActiveFor(UserModel user) {
        List<SectorModel> allSectorList = getAll();

        if (user == null || user.getSectors() == null || user.getSectors().isEmpty()) {
            return allSectorList;
        }

        List<SectorModel> userSectorList = user.getSectors();
        for (SectorModel sector : allSectorList) {
            if (userSectorList.contains(sector)) {
                sector.setActive(true);
            }
        }

        return allSectorList;
    }
}
