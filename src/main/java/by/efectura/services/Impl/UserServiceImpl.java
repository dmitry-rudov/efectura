package by.efectura.services.Impl;

import by.efectura.models.UserModel;
import by.efectura.repositories.UserRepository;
import by.efectura.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserModel add(UserModel user) {
        return userRepository.save(user);
    }

    @Override
    public UserModel update(UserModel user) {
        UserModel updateUser = getBySession(user.getSession());
        updateUser.setAgree(user.isAgree());
        updateUser.setSectors(user.getSectors());
        updateUser.setName(user.getName());

        return userRepository.save(updateUser);
    }

    @Override
    public UserModel getBySession(String session) {
        return userRepository.findBySession(session);
    }

    @Override
    public UserModel save(UserModel user, String session) {
        UserModel doubleUser = getBySession(session);
        user.setSession(session);

        if (doubleUser != null) {
            user = update(user);
        } else {
            user = add(user);
        }
        return user;
    }
}
