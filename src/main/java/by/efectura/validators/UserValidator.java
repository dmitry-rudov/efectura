package by.efectura.validators;

import by.efectura.models.UserModel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static by.efectura.constants.ValidationConstants.*;

@Component
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return UserModel.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        UserModel user = (UserModel) object;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,
                FIELD_FOR_NAME_USER,
                CODE_ERROR_NULL_OR_EMPTY_FOR_NAME_USER,
                MESSAGE_NULL_OR_EMPTY_FOR_NAME_USER);

        String name = user.getName();
        if (name != null && name.length() < MIN_CHARACTER_FOR_NAME_USER) {
            errors.rejectValue(FIELD_FOR_NAME_USER,
                    CODE_ERROR_MIN_CHARACTER_FOR_NAME_USER,
                    MESSAGE_MIN_CHARACTER_FOR_NAME_USER);
        }

        if (!user.isAgree()) {
            errors.rejectValue(FIELD_FOR_AGREE_USER, CODE_ERROR_NOT_AGREE_FOR_AGREE_USER, MESSAGE_NOT_AGREE_FOR_AGREE_USER);
        }

        ValidationUtils.rejectIfEmpty(errors,
                FIELD_FOR_SECTOR,
                CODE_ERROR_NULL_OR_EMPTY_FOR_SECTOR,
                MESSAGE_NULL_OR_EMPTY_FOR_SECTOR);
    }
}
