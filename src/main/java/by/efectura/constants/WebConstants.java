package by.efectura.constants;

public final class WebConstants {
    public final static String INDEX_PAGE = "index";
    public final static String ATTRIBUTE_SECTORS = "sectorList";
    public final static String ATTRIBUTE_USER = "user";
}
