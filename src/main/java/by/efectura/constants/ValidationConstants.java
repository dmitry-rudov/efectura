package by.efectura.constants;

public final class ValidationConstants {
    public final static String FIELD_FOR_NAME_USER = "name";
    public final static String CODE_ERROR_NULL_OR_EMPTY_FOR_NAME_USER = "user.name.empty";
    public final static String MESSAGE_NULL_OR_EMPTY_FOR_NAME_USER = "Username must not be empty.";

    public static final int MIN_CHARACTER_FOR_NAME_USER = 2;
    public static final String CODE_ERROR_MIN_CHARACTER_FOR_NAME_USER = "user.name.short";
    public final static String MESSAGE_MIN_CHARACTER_FOR_NAME_USER ="Username must more than one character.";

    public final static String FIELD_FOR_AGREE_USER = "agree";
    public final static String CODE_ERROR_NOT_AGREE_FOR_AGREE_USER = "user.not.agree";
    public final static String MESSAGE_NOT_AGREE_FOR_AGREE_USER = "You must agree with the terms";

    public final static String FIELD_FOR_SECTOR = "sectors";
    public final static String CODE_ERROR_NULL_OR_EMPTY_FOR_SECTOR = "sectors.empty";
    public final static String MESSAGE_NULL_OR_EMPTY_FOR_SECTOR = "You must select at least one item";
}
