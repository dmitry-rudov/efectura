package by.efectura.controllers;

import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;
import by.efectura.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import by.efectura.services.SectorService;
import by.efectura.services.UserService;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static by.efectura.constants.WebConstants.ATTRIBUTE_SECTORS;
import static by.efectura.constants.WebConstants.ATTRIBUTE_USER;
import static by.efectura.constants.WebConstants.INDEX_PAGE;

@Controller
public class FormController {

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private SectorService sectorService;

    @Autowired
    private UserService userService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(userValidator);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcomeFormPage(Model model, HttpServletRequest request) {

        List<SectorModel> sectorList = sectorService.getAll();
        UserModel user = userService.getBySession(request.getSession().getId());
        if (user == null) {
            user = new UserModel();
        }


        model.addAttribute(ATTRIBUTE_SECTORS, sectorList);
        model.addAttribute(ATTRIBUTE_USER, user);

        return INDEX_PAGE;
    }

    @RequestMapping(value = "/saveForm", method = RequestMethod.POST)
    public String saveFormPage(@ModelAttribute("user") @Valid UserModel user, BindingResult bindingResult,
                               Model model, HttpServletRequest request) {

        List<SectorModel> allSectorList = sectorService.getAllAndMarkActiveFor(user);

        model.addAttribute(ATTRIBUTE_SECTORS,allSectorList);

        if (bindingResult.hasErrors()) {
            return INDEX_PAGE;
        }

        userService.save(user, request.getSession().getId());

        return INDEX_PAGE;
    }
}

