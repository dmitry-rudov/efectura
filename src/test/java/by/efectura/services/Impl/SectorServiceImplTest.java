package by.efectura.services.Impl;

import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;
import by.efectura.repositories.SectorRepository;
import by.efectura.services.SectorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class SectorServiceImplTest {

    @TestConfiguration
    static class SectorServiceImplTestContextConfiguration {

        @Bean
        public SectorService sectorService() {
            return new SectorServiceImpl();
        }
    }

    @Autowired
    private SectorService sectorService;

    @MockBean
    private SectorRepository sectorRepository;

    private UserModel user;
    private List<SectorModel> sectors = new ArrayList<SectorModel>();

    @Before
    public void setUp() {
        user = new UserModel();
        SectorModel sectorOne = new SectorModel();
        SectorModel sectorTwo = new SectorModel();
        SectorModel sectorThree = new SectorModel();
        SectorModel sectorFour = new SectorModel();

        sectorOne.setId(1L);
        sectorOne.setActive(false);
        sectorOne.setName("test sector one");
        sectorOne.setSubCategory(Arrays.asList(sectorFour));
        sectorOne.setUsers(Arrays.asList(user));

        sectorTwo.setId(2L);
        sectorTwo.setActive(false);
        sectorTwo.setSubCategory(Arrays.asList(sectorThree));
        sectorTwo.setName("test sector two");

        sectorThree.setId(3L);
        sectorThree.setActive(false);
        sectorThree.setName("test sector three");

        sectorFour.setId(4L);
        sectorFour.setActive(false);
        sectorFour.setName("test sector four");

        user.setId(1L);
        user.setName("test");
        user.setAgree(true);
        user.setSession("1");
        user.setSectors(Arrays.asList(sectorOne));


        when(sectorRepository.findByParentIsNull()).thenReturn(Arrays.asList(sectorOne, sectorTwo));
    }

    @Test
    public void testGetAll() throws Exception {
        List<SectorModel> sectors = sectorService.getAll();

        assertThat(sectors.size(), equalTo(4));
        assertThat(sectors.get(0).getId(), equalTo(1L));
        assertThat(sectors.get(1).getId(), equalTo(4L));
        assertThat(sectors.get(2).getId(), equalTo(2L));
        assertThat(sectors.get(3).getId(), equalTo(3L));
    }

    @Test
    public void testGetAllAndMarkActiveFor() throws Exception {
        List<SectorModel> sectors = sectorService.getAllAndMarkActiveFor(user);

        assertThat(sectors.get(0).isActive(),equalTo(true));
        assertThat(sectors.get(1).isActive(),equalTo(false));
        assertThat(sectors.get(2).isActive(),equalTo(false));
        assertThat(sectors.get(3).isActive(),equalTo(false));
    }

}