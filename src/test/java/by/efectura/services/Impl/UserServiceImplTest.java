package by.efectura.services.Impl;

import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;
import by.efectura.repositories.UserRepository;
import by.efectura.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @TestConfiguration
    static class UserServiceImplTestContextConfiguration {

        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    private UserModel userFromWebFormAdd;
    private UserModel userFromWebFormUpdate;
    private UserModel userFromWebFormNew;

    @Before
    public void setUp() {
        UserModel user = new UserModel();
        SectorModel sectorOne = new SectorModel();
        SectorModel sectorTwo = new SectorModel();

        sectorOne.setId(1L);
        sectorOne.setActive(false);
        sectorOne.setName("test sector one");
        sectorOne.setUsers(Arrays.asList(user));

        sectorTwo.setId(2L);
        sectorTwo.setActive(false);
        sectorTwo.setName("test sector two");

        user.setId(1L);
        user.setName("test");
        user.setAgree(true);
        user.setSession("1");
        user.setSectors(Arrays.asList(sectorOne));

        userFromWebFormAdd = new UserModel();
        userFromWebFormAdd.setName("test");
        userFromWebFormAdd.setAgree(true);
        userFromWebFormAdd.setSession("1");
        userFromWebFormAdd.setSectors(Arrays.asList(sectorOne));

        userFromWebFormUpdate = new UserModel();
        userFromWebFormUpdate.setName("test test");
        userFromWebFormUpdate.setAgree(true);
        userFromWebFormUpdate.setSession("1");
        userFromWebFormUpdate.setSectors(Arrays.asList(sectorOne));

        userFromWebFormNew = new UserModel();
        userFromWebFormNew.setName("new test");
        userFromWebFormNew.setAgree(true);
        userFromWebFormNew.setSectors(Arrays.asList(sectorOne));

        when(userRepository.save(userFromWebFormAdd)).thenReturn(user);
        when(userRepository.save(userFromWebFormNew)).thenReturn(userFromWebFormNew);
        when(userRepository.save(user)).thenReturn(user);
        when(userRepository.findBySession("1")).thenReturn(user);
    }

    @Test
    public void testAddNewUser() throws Exception {
        UserModel newUser = userService.add(userFromWebFormAdd);

        assertThat(newUser.getId(), is(equalTo(1L)));
    }



    @Test
    public void testUpdateOldUser() throws Exception {
        UserModel updateUser = userService.update(userFromWebFormUpdate);

        assertThat(updateUser.getName(), equalTo("test test"));
    }

    @Test
    public void testGetBySession() throws Exception {
        UserModel userBySession = userService.getBySession("1");

        assertThat(userBySession.getId(), equalTo(1L));
    }

    @Test
    public void testSaveWhenNewSession() throws Exception {
        UserModel userSave = userService.save(userFromWebFormNew, "2");

        assertThat(userSave.getSession(), equalTo("2"));
        assertThat(userSave.getName(), equalTo("new test"));
    }

    @Test
    public void testSaveWhenOldSession() throws Exception {
        UserModel userSave = userService.save(userFromWebFormNew, "1");

        assertThat(userSave.getSession(), equalTo("1"));
        assertThat(userSave.getName(), equalTo("new test"));
    }

}