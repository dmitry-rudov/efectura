package by.efectura.controllers;

import by.efectura.models.SectorModel;
import by.efectura.services.SectorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FormControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private SectorService sectorService;


    private SectorModel sectorOne;
    private SectorModel sectorTwo;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

        sectorOne = new SectorModel();
        sectorOne.setId(1L);
        sectorOne.setName("one test name");
        sectorOne.setActive(false);

        sectorTwo = new SectorModel();
        sectorTwo.setId(2L);
        sectorTwo.setName("two test name");
        sectorTwo.setActive(false);

        when(sectorService.getAll()).thenReturn(Arrays.asList(sectorOne, sectorTwo));

    }

    @Test
    public void testWelcomeFormPage() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("sectorList", hasSize(2)))
                .andExpect(model().attribute("sectorList", hasItem(
                        allOf(
                                hasProperty("id", is(1L)),
                                hasProperty("name", is("one test name")),
                                hasProperty("active", is(false))
                        )
                )))
                .andExpect(model().attribute("sectorList", hasItem(
                        allOf(
                                hasProperty("id", is(2L)),
                                hasProperty("name", is("two test name")),
                                hasProperty("active", is(false))
                        )
                )));

        verify(sectorService, times(1)).getAll();
    }

    @Test
    public void testSaveFormPage() throws Exception {

        mockMvc.perform(post("/saveForm")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "test")
                .param("sectors", "2")
                .param("agree", "true"))

                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("user",
                        allOf(
                                hasProperty("name", is("test")),
                                hasProperty("agree", is(true))
                        )
                ));

    }

}