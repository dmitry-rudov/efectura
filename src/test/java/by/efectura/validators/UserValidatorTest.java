package by.efectura.validators;

import by.efectura.constants.ValidationConstants;
import by.efectura.models.SectorModel;
import by.efectura.models.UserModel;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class UserValidatorTest {

    private UserModel testUserRight;
    private UserModel testUserWhereNameIsNull;
    private UserModel testUserWhereNameIsWhitespace;
    private UserModel testUserWhereNameLengthLessTwo;
    private UserModel testUserWhereAgreeIsFalse;
    private UserModel testUserWhereSectorsIsEmpty;

    @Before
    public void init() {
        SectorModel sector = new SectorModel();

        testUserRight = new UserModel();
        testUserRight.setName("testRight");
        testUserRight.setAgree(true);
        testUserRight.setSectors(Arrays.asList(sector));

        testUserWhereNameIsNull = new UserModel();
        testUserWhereNameIsNull.setAgree(true);
        testUserWhereNameIsNull.setSectors(Arrays.asList(sector));

        testUserWhereNameIsWhitespace = new UserModel();
        testUserWhereNameIsWhitespace.setName("    ");
        testUserWhereNameIsWhitespace.setAgree(true);
        testUserWhereNameIsWhitespace.setSectors(Arrays.asList(sector));

        testUserWhereNameLengthLessTwo = new UserModel();
        testUserWhereNameLengthLessTwo.setName("D");
        testUserWhereNameLengthLessTwo.setAgree(true);
        testUserWhereNameLengthLessTwo.setSectors(Arrays.asList(sector));

        testUserWhereAgreeIsFalse = new UserModel();
        testUserWhereAgreeIsFalse.setName("test");
        testUserWhereAgreeIsFalse.setAgree(false);
        testUserWhereAgreeIsFalse.setSectors(Arrays.asList(sector));

        testUserWhereSectorsIsEmpty = new UserModel();
        testUserWhereSectorsIsEmpty.setName("test");
        testUserWhereSectorsIsEmpty.setAgree(true);
        testUserWhereSectorsIsEmpty.setSectors(null);

    }

    @Test
    public void testValidateWhereUserIsRight() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserRight, "user");
        userValidator.validate(testUserRight, errors);
        assertEquals(0, errors.getErrorCount());
    }

    @Test
    public void testValidateWhereNameUserIsNull() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserWhereNameIsNull, "user");
        userValidator.validate(testUserWhereNameIsNull, errors);
        assertEquals(ValidationConstants.CODE_ERROR_NULL_OR_EMPTY_FOR_NAME_USER, errors.getFieldError().getCode());
    }

    @Test
    public void testValidateWhereNameUserIsWhitespace() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserWhereNameIsWhitespace, "user");
        userValidator.validate(testUserWhereNameIsWhitespace, errors);
        assertEquals(ValidationConstants.CODE_ERROR_NULL_OR_EMPTY_FOR_NAME_USER, errors.getFieldError().getCode());
    }

    @Test
    public void testValidateWhereNameUserLenghtIsLessTwo() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserWhereNameLengthLessTwo, "user");
        userValidator.validate(testUserWhereNameLengthLessTwo, errors);
        assertEquals(ValidationConstants.CODE_ERROR_MIN_CHARACTER_FOR_NAME_USER, errors.getFieldError().getCode());
    }

    @Test
    public void testValidateWhereAgreeUserIsFalse() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserWhereAgreeIsFalse, "user");
        userValidator.validate(testUserWhereAgreeIsFalse, errors);
        assertEquals(ValidationConstants.CODE_ERROR_NOT_AGREE_FOR_AGREE_USER, errors.getFieldError().getCode());
    }

    @Test
    public void testValidateWhereSectorsIsEmpty() throws Exception {
        UserValidator userValidator = new UserValidator();
        Errors errors = new BeanPropertyBindingResult(testUserWhereSectorsIsEmpty, "sectors");
        userValidator.validate(testUserWhereSectorsIsEmpty, errors);
        assertEquals(ValidationConstants.CODE_ERROR_NULL_OR_EMPTY_FOR_SECTOR, errors.getFieldError().getCode());
    }

}